/*
 * a simple kernel module: hello char
 *
 * Copyright (C) 2014 Barry Song  (baohua@kernel.org)
 *
 * Licensed under GPLv2 or later.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/platform_device.h>

#define DRIVER_NAME "hello_char_old"
#define HELLO_CHAR_MAJOR 55

static int hello_char_open(struct inode *inode, struct file *filp)
{
    pr_err("hello char open\n");
    return 0;
}

static const struct file_operations hello_char_fops = {
    .open       = hello_char_open,
    .owner      = THIS_MODULE,
};

static int __init hello_char_init(void)
{
    int ret;

    printk(KERN_DEBUG "hello char init\n");

    ret = register_chrdev(HELLO_CHAR_MAJOR, DRIVER_NAME, &hello_char_fops);
    if (ret < 0) {
        printk(KERN_ERR "unable to register char device\n");
        goto error;
    }

    return 0;
error:
    return ret;
}

static void __exit hello_char_exit(void)
{
    pr_debug("exit form hello!\n");
    unregister_chrdev(HELLO_CHAR_MAJOR, DRIVER_NAME);
}

module_init(hello_char_init);
module_exit(hello_char_exit);

