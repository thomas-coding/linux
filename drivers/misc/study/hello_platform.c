// SPDX-License-Identifier: GPL-2.0-only
/*
 * a simple kernel module: hello char
 *
 * Copyright (C) 2022 Jinping Wu  (jinping.wu@gmail.com)
 *
 * Licensed under GPLv2 or later.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>

static int hello_platform_probe(struct platform_device *pdev)
{
	pr_err("hello platform probe\n");
	return 0;
}

static int hello_platform_remove(struct platform_device *pdev)
{
	pr_err("hello platform remove 111\n");
	return 0;
}

static const struct of_device_id hello_platform_id_table[] = {
	{ .compatible = "hello_platform"},
	{ }
};
MODULE_DEVICE_TABLE(of, hello_platform_id_table);

static struct platform_driver hello_platform_driver = {
	.driver     = {
		.name   = "hello_platform",
		.of_match_table = of_match_ptr(hello_platform_id_table),
	},
	.probe      = hello_platform_probe,
	.remove     = hello_platform_remove,
};

module_platform_driver(hello_platform_driver);

MODULE_DESCRIPTION("hello_platform driver");
MODULE_AUTHOR("Jinping Wu <wunekky@gmail.com>");
MODULE_LICENSE("GPL");
